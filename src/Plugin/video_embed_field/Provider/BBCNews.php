<?php

namespace Drupal\video_embed_bbcnews\Plugin\video_embed_field\Provider;

use Drupal\video_embed_field\ProviderPluginBase;
use Drupal\opengraph_utility\OpenGraphService;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @VideoEmbedProvider(
 *   id = "bbcnews",
 *   title = @Translation("BBC News")
 * )
 */
class BBCNews extends ProviderPluginBase {

    /**
     * @var array|null
     */
    protected $ogService;

    /**
     * {@inheritdoc}
     */
    public function __construct($configuration, $plugin_id, $plugin_definition, ClientInterface $http_client, OpenGraphService $service) {
        parent::__construct($configuration, $plugin_id, $plugin_definition, $http_client);
        $this->ogService = $service;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static($configuration, $plugin_id, $plugin_definition, $container->get('http_client'), $container->get('opengraph'));
    }
    
    /**
     * {@inheritdoc}
     */
    public function renderEmbedCode($width, $height, $autoplay) {
        return [
            '#type' => 'html_tag',
            '#tag' => 'iframe',
            '#attributes' => [
                'width' => $width,
                'height' => $height,
                'frameborder' => '0',
                'allowfullscreen' => 'allowfullscreen',
                'scrolling' => 'no',
                'src' => '//www.bbc.co.uk/' . $this->getVideoId() . '/embed',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getLocalThumbnailUri() {
        return $this->thumbsDirectory . '/' . str_replace('/', '-', $this->getVideoId()) . '.jpg';
    }
    
    /**
     * {@inheritdoc}
     */
    public function getRemoteThumbnailUrl() {
        //$ogService = new OpenGraphService();
        $tags = $this->ogService->getOpenGraphInfo('//www.bbc.co.uk/' . $this->getVideoId() . '/embed', array('image'));
        
        return $tags['image'];
    }

    /**
     * {@inheritdoc}
     */
    public static function getIdFromInput($input) {
        if (preg_match('/bbc.co.uk\/([a-zA-Z0-9\/-]*)/i', $input, $matches)) {
            return $matches[1];
        }
    }

}
